const express = require('express')
const router = express.Router()
const users = require("../data/users.json")

router.get("/login",(req, res)=>{
    res.render("login", {error: null})
});

router.post("/login",(req, res)=>{
    // console.log("DATA >>>>", req.body);
    const username = req.body.username;
    const password = req.body.password;
    // console.log(users);
    const findUser = users.find((user) => user.username === username && user.password === password)
    if (!findUser) {
        res.render("login", { error: "Invalid Login"})
    }
    res.redirect("./dashboard")
   
});

module.exports = router